About Content Entity Language Switcher

The module consists of a single hook, which alters language links in Language Switcher blocks on node pages.
It displays only those, which are node translated. If there is only one language, language switcher will not be shown.